### System requirements
Successfully run on:
- `sbt version 1.6.2`
- `docker version 20.10.16`
- `docker compose version 2.6.0`
- `java version 18.0.1.1`

### How to run:
1. go to project directory
2. run docker containers with postgres database  
`docker compose -f src/it/resources/docker-compose.yml up -d`
3. run application  
`sbt run`


### How to run tests:
1. go to project directory
2. run `sbt it:test`

By default, the application starts an http server on port `8080`

### Changing default configuration
Environmental variables can be used to change default configuration:
- `HTTP_PORT` - port used by http server (default: `8080`)
- `DB_URL` - url to database (default: `jdbc:postgresql://localhost:5432/simple_app_db`)
- `DB_USERNAME` - username to database (default: `user`)
- `DB_PASSWORD` - password to database (default: `pass`)


### Notes:
I wasn't sure how to model a failed validation. The specification says that a failed validation results in an "error" response.

I decided to be consistent with the status semantics and modeled this as an application error. However, if I were working on a project like this, I would suggest changing the response to success with some sort of `"valid": false` field,
because there is no problem with the user input or the application itself.
