package com.bszmit.simpleapp.domain

import io.circe.Json

sealed trait Err

object Err {
  final case class SchemaIdNotUnique(id: SchemaId)              extends Err
  final case object InvalidSchema                               extends Err
  final case object InvalidJson                                 extends Err
  final case class SchemaDoesNotExist(id: SchemaId)             extends Err
  final case class ValidationFailed(id: SchemaId, errors: Json) extends Err
  final case class DatabaseError(t: Throwable)                  extends Err
  final case class InternalError(t: Throwable)                  extends Err
}
