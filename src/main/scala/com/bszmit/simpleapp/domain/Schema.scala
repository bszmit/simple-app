package com.bszmit.simpleapp.domain

import io.circe.Json

final case class Schema(v: Json)     extends AnyVal
final case class SchemaId(v: String) extends AnyVal
