package com.bszmit.simpleapp.domain

import zio.ZIO
import zio.logging.Logging

trait SchemaRepository {
  def register(id: SchemaId, schema: Schema): ZIO[Logging, Err, Unit]
  def getById(id: SchemaId): ZIO[Logging, Err, Option[Schema]]
}
