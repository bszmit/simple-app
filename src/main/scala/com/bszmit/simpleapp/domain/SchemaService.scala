package com.bszmit.simpleapp.domain

import com.fasterxml.jackson.databind.JsonNode
import com.github.fge.jackson.JsonLoader
import com.github.fge.jsonschema.core.report.{ListProcessingReport, ListReportProvider, LogLevel}
import com.github.fge.jsonschema.main.{JsonSchema, JsonSchemaFactory}
import io.circe.Json as CirceJson
import Err.{InternalError, InvalidSchema, SchemaDoesNotExist, ValidationFailed}
import zio.logging.{Logging, log}
import zio.{Function1ToLayerSyntax, Has, IO, RLayer, UIO, ZIO}

import scala.util.{Failure, Success, Try}

class SchemaService(repo: SchemaRepository) {
  private val schemaFactory: JsonSchemaFactory =
    JsonSchemaFactory.newBuilder().setReportProvider(new ListReportProvider(LogLevel.INFO, LogLevel.FATAL)).freeze()

  def uploadSchema(id: SchemaId, schema: Schema): ZIO[Logging, Err, Unit] =
    log.info(s"Uploading schema with id: ${id.v}") *> repo.register(id, schema)

  def fetchSchema(id: SchemaId): ZIO[Logging, Err, Schema] = log.info(s"Fetching schema with id: ${id.v}") *> repo.getById(id).flatMap {
    case Some(schema) => IO.succeed(schema)
    case None         => IO.fail(SchemaDoesNotExist(id))
  }

  def validate(id: SchemaId, document: CirceJson): ZIO[Logging, Err, CirceJson] = for {
    _             <- log.info(s"Validating document with schema ${id.v}")
    schema        <- fetchSchema(id)
    jacksonSchema <- schema.v.toJackson >>= toJsonSchema
    cleanDoc = cleanEmptyValues(document)
    cleanDocJackson <- cleanDoc.toJackson
    _               <- doValidate(cleanDocJackson, id, jacksonSchema)
  } yield cleanDoc

  private def toJsonSchema(jsonNode: JsonNode): IO[InvalidSchema.type, JsonSchema] = Try(schemaFactory.getJsonSchema(jsonNode)) match {
    case Failure(_)     => IO.fail(InvalidSchema)
    case Success(value) => IO.succeed(value)
  }

  private def doValidate(doc: JsonNode, schemaId: SchemaId, schema: JsonSchema): IO[Err, Unit] =
    Try(schema.validate(doc)) match {
      case Success(r: ListProcessingReport) =>
        if (r.isSuccess) IO.unit else r.asJson().toCirce.flatMap(errs => IO.fail(ValidationFailed(schemaId, errs)))
      case Success(_)  => IO.dieMessage("unexpected reporter")
      case Failure(ex) => IO.fail(InternalError(ex))
    }

  private def cleanEmptyValues(json: CirceJson): CirceJson = json.deepDropNullValues

  private implicit class JacksonOps(j: JsonNode) {
    def toCirce: UIO[CirceJson] = io.circe.parser.parse(j.toString) match {
      case Left(_)      => IO.dieMessage("Couldn't convert json")
      case Right(value) => IO.succeed(value)
    }
  }

  private implicit class CirceOps(j: CirceJson) {
    def toJackson: UIO[JsonNode] = Try(JsonLoader.fromString(j.noSpaces)) match {
      case Failure(_)     => IO.dieMessage("Couldn't convert json")
      case Success(value) => IO.succeed(value)
    }
  }
}

object SchemaService {
  val layer: RLayer[Has[SchemaRepository], Has[SchemaService]] = (new SchemaService(_)).toLayer
}
