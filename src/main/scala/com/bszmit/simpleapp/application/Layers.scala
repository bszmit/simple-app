package com.bszmit.simpleapp.application

import com.bszmit.simpleapp.application.http.{HttpRouter, SchemaRoutes}
import com.bszmit.simpleapp.domain.SchemaService
import com.bszmit.simpleapp.infrastructure.db.Database
import com.bszmit.simpleapp.infrastructure.repositories.DbSchemaRepository
import zio.ZLayer
import zio.logging.slf4j.Slf4jLogger

object Layers {
  val loggerLayer     = Slf4jLogger.make((_, msg) => msg)
  val appConfig       = AppConfig.layer
  val repositoryLayer = ZLayer.service[AppConfig].flatMap(conf => Database.layer(conf.get.db) >>> DbSchemaRepository.layer)
  val serviceLayer    = SchemaService.layer
  val httpRoutesLayer = SchemaRoutes.layer >>> HttpRouter.layer
}
