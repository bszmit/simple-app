package com.bszmit.simpleapp.application

import com.bszmit.simpleapp.application.http.HttpRouter
import com.bszmit.simpleapp.infrastructure.HttpServer
import zio.logging.log
import zio.magic.*
import zio.{ExitCode, URIO, ZIO}

object Main extends zio.App {

  def run(args: List[String]): URIO[zio.ZEnv, ExitCode] = {

    val eff = for {
      config     <- ZIO.service[AppConfig]
      router     <- ZIO.service[HttpRouter]
      _          <- log.info(s"Starting HTTP server with config $config")
      httpServer <- HttpServer.runHttpServer(config.http.port)(router.router)
    } yield httpServer

    eff.injectCustom(Layers.loggerLayer, Layers.appConfig, Layers.repositoryLayer, Layers.serviceLayer, Layers.httpRoutesLayer).exitCode
  }

}
