package com.bszmit.simpleapp.application.http

import cats.data.EitherT
import com.bszmit.simpleapp.domain.Err.InvalidJson
import com.bszmit.simpleapp.domain.{Err, Schema, SchemaId, SchemaService}
import com.bszmit.simpleapp.infrastructure.serdes.SchemaCodec
import io.circe.{Json, ParsingFailure}
import org.http4s.*
import org.http4s.circe.jsonDecoder
import org.http4s.dsl.Http4sDsl
import HttpResponse.{respWithData, respWithoutData}
import com.bszmit.simpleapp.infrastructure.ErrorLogger
import com.bszmit.simpleapp.infrastructure.Utils.{CtxOpsUIO, TapLeftEitherT}
import zio.interop.catz.*
import zio.logging.Logging
import zio.{Function1ToLayerSyntax, Has, IO, RIO, RLayer}

class SchemaRoutes(service: SchemaService) {
  type SchemaRoutesEff[A] = RIO[Logging, A]

  private val dsl: Http4sDsl[SchemaRoutesEff] = Http4sDsl[SchemaRoutesEff]
  import dsl.*

  val routes = HttpRoutes.of[SchemaRoutesEff] {
    case req @ POST -> Root / "schema" / schemaId =>
      val r = for {
        schema <- EitherT(parseInput[Json](req))
        resp   <- EitherT[SchemaRoutesEff, Err, Unit](service.uploadSchema(SchemaId(schemaId), Schema(schema)).either)
      } yield resp
      r.tapLeft(ErrorLogger.logError).value.map(resp => respWithoutData("uploadSchema", schemaId, Status.Created, resp).widen)

    case GET -> Root / "schema" / schemaId =>
      val r = for {
        resp <- EitherT[SchemaRoutesEff, Err, Schema](service.fetchSchema(SchemaId(schemaId)).either)
      } yield resp
      r.tapLeft(ErrorLogger.logError).value.map(resp => respWithData("downloadSchema", schemaId, Status.Ok, resp)(SchemaCodec.enc).widen)

    case req @ POST -> Root / "validate" / schemaId =>
      val r = for {
        document <- EitherT(parseInput[Json](req))
        resp     <- EitherT[SchemaRoutesEff, Err, Json](service.validate(SchemaId(schemaId), document).either)
      } yield resp
      r.tapLeft(ErrorLogger.logError).value.map(resp => respWithData("validateDocument", schemaId, Status.Created, resp).widen)
  }

  private def parseInput[T: EntityDecoder[SchemaRoutesEff, *]](req: Request[SchemaRoutesEff]): SchemaRoutesEff[Either[Err, T]] =
    req.as[T].map(Right(_)).catchSome {
      case _: ParsingFailure              => IO.succeed(Left(InvalidJson))
      case _: MalformedMessageBodyFailure => IO.succeed(Left(InvalidJson))
    }

  // we're working with monad transformers EitherT, since Http4s requires Effect with Throwable as an error but we want to maintain the Err error type
}

object SchemaRoutes {
  val layer: RLayer[Has[SchemaService], Has[SchemaRoutes]] = (new SchemaRoutes(_)).toLayer
}
