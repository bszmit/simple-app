package com.bszmit.simpleapp.application.http

import cats.data.Kleisli
import org.http4s.server.Router
import org.http4s.{HttpApp, Response, Status}
import HttpResponse.customError
import com.bszmit.simpleapp.infrastructure.Utils.CtxOpsUIO
import zio.interop.catz.*
import zio.logging.Logging
import zio.{Function1ToLayerSyntax, Has, RIO, RLayer, ZIO}

class HttpRouter(schemaRoutes: SchemaRoutes) {
  type Eff[A] = RIO[Logging, A]

  private def errorHandler[R]: Throwable => RIO[R, Response[RIO[R, *]]] = { case e: Throwable =>
    e.printStackTrace()
    ZIO.succeed(customError("Internal server error").widen)
  }

  val router: HttpApp[Eff] = {
    val r = Router[Eff](
      "/" -> schemaRoutes.routes,
    ).mapF(_.getOrElse(Response[Eff](Status.NotFound)))

    Kleisli(req => r.run(req).catchAll(errorHandler)) // needed, so we can do unit tests of endpoints without running blaze backend
  }

}

object HttpRouter {
  val layer: RLayer[Has[SchemaRoutes], Has[HttpRouter]] = (new HttpRouter(_)).toLayer
}
