package com.bszmit.simpleapp.application.http

import com.bszmit.simpleapp.domain.Err
import io.circe.{Encoder, Json}
import org.http4s.circe.CirceEntityCodec.circeEntityEncoder
import org.http4s.circe.jsonEncoder
import org.http4s.{Response, Status}
import zio.UIO

object HttpResponse {

  def customError(msg: String): Response[UIO] = {
    val body = Json.obj(
      "status"  -> Json.fromString("error"),
      "message" -> Json.fromString(msg),
    )
    Response(Status.InternalServerError).withEntity(body)
  }

  private def errToResponse(action: String, id: String, err: Err): Response[UIO] = {
    def errBody(msg: Json): Json = Json.obj(
      "action"  -> Json.fromString(action),
      "id"      -> Json.fromString(id),
      "status"  -> Json.fromString("error"),
      "message" -> msg,
    )

    val (status, msg) = err match {
      case Err.SchemaIdNotUnique(_)      => (Status.Conflict, Json.fromString("Schema with a given id already exists"))
      case Err.InvalidSchema             => (Status.BadRequest, Json.fromString("Invalid schema"))
      case Err.InvalidJson               => (Status.BadRequest, Json.fromString("Invalid JSON"))
      case Err.ValidationFailed(_, errs) => (Status.BadRequest, errs)
      case Err.SchemaDoesNotExist(_)     => (Status.NotFound, Json.fromString("Schema does not exist"))
      case Err.DatabaseError(_)          => (Status.InternalServerError, Json.fromString("Internal error"))
      case Err.InternalError(_)          => (Status.InternalServerError, Json.fromString("Internal error"))
    }
    Response(status = status).withEntity(errBody(msg))
  }

  private def okToResponse(action: String, id: String, status: Status, maybeData: Option[Json]): Response[UIO] = {
    val body = Map(
      "action" -> Json.fromString(action),
      "id"     -> Json.fromString(id),
      "status" -> Json.fromString("success"),
    )
    val bodyWithData = maybeData match {
      case Some(value) => body + ("data" -> value)
      case None        => body
    }
    Response(status = status).withEntity(bodyWithData)
  }

  def respWithData[T: Encoder](action: String, id: String, successStatus: Status, eith: Either[Err, T]): Response[UIO] =
    eith.fold(err => errToResponse(action, id, err), data => okToResponse(action, id, successStatus, Some(Encoder[T].apply(data))))

  def respWithoutData(action: String, id: String, successStatus: Status, eith: Either[Err, _]): Response[UIO] =
    eith.fold(err => errToResponse(action, id, err), _ => okToResponse(action, id, successStatus, None))

}
