package com.bszmit.simpleapp.application

import cats.implicits.*
import com.typesafe.config.{Config, ConfigFactory}
import zio.interop.catz.*
import zio.{Has, Task, UIO, ULayer}

object AppConfig {
  private def readDbConfig(db: Config): Task[DBConfig] = Task.effect {
    val driver   = db.getString("driver")
    val url      = db.getString("url")
    val username = db.getString("username")
    val password = db.getString("password")
    DBConfig(driver, url, username, password)
  }

  private def readHttpConfig(http: Config): Task[HttpConfig] = Task.effect {
    val port = http.getInt("port")

    HttpConfig(port)
  }

  private val appConfig: UIO[AppConfig] = Task
    .effect {
      val config = ConfigFactory.load()

      val dbConfig   = readDbConfig(config.getConfig("db"))
      val httpConfig = readHttpConfig(config.getConfig("http"))

      (dbConfig, httpConfig).mapN(AppConfig(_, _))
    }
    .flatten
    .catchAll(t => Task.dieMessage(s"Invalid config: ${t.getMessage}"))

  val layer: ULayer[Has[AppConfig]] = appConfig.toLayer
}

final case class DBConfig(driver: String, url: String, username: String, password: String)
final case class HttpConfig(port: Int)
final case class AppConfig(db: DBConfig, http: HttpConfig)
