package com.bszmit.simpleapp.infrastructure.repositories

import com.bszmit.simpleapp.domain.{Err, Schema, SchemaId, SchemaRepository}
import doobie.*
import doobie.implicits.*
import doobie.postgres.circe.json.implicits.{jsonGet, jsonPut}
import doobie.postgres.sqlstate
import io.circe.Json
import org.postgresql.util.PSQLException
import zio.interop.catz.*
import zio.logging.{Logging, log}
import zio.{Has, RLayer, Task, ZIO}

class DbSchemaRepository(tx: Transactor[Task]) extends SchemaRepository {

  def register(id: SchemaId, schema: Schema): ZIO[Logging, Err, Unit] = {
    val query = sql"""
      INSERT INTO schemas (id, schema)
      VALUES (${id.v}, $schema)
      """.update.run.map(_ => ())

    executeQuery(query).catchSome {
      case Err.DatabaseError(t: PSQLException) if t.getSQLState == sqlstate.class23.UNIQUE_VIOLATION.value =>
        ZIO.fail(Err.SchemaIdNotUnique(id))
    }
  }

  def getById(id: SchemaId): ZIO[Logging, Err, Option[Schema]] = {
    val query = sql"""
            SELECT schema
            FROM schemas
            WHERE id = ${id.v}"""
      .query[Schema]
      .option

    executeQuery(query)
  }

  private def executeQuery[A](c: ConnectionIO[A]): ZIO[Logging, Err, A] =
    c.transact(tx)
      .tapError(e => log.throwable("Executing db query failed with error", e))
      .mapError(Err.DatabaseError)

  private implicit val p1: Put[Schema] = Put[Json].contramap(_.v)
  private implicit val g1: Get[Schema] = Get[Json].map(Schema)

}

object DbSchemaRepository {
  val layer: RLayer[Has[Transactor[Task]], Has[SchemaRepository]] = (new DbSchemaRepository(_)).toLayer
}
