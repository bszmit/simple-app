package com.bszmit.simpleapp.infrastructure

import com.bszmit.simpleapp.domain.Err
import zio.URIO
import zio.logging.{Logging, log}

object ErrorLogger {

  def logError(err: Err): URIO[Logging, Unit] = err match {
    case Err.SchemaIdNotUnique(id)   => log.error(s"Schema id ${id.v} is not unique")
    case Err.InvalidSchema           => log.error(s"Schema is invalid")
    case Err.InvalidJson             => log.error(s"JSON is invalid")
    case Err.SchemaDoesNotExist(id)  => log.error(s"Schema with id ${id.v} does not exist")
    case Err.ValidationFailed(id, _) => log.error(s"Document's validation with schema ${id.v} failed")
    case Err.DatabaseError(t)        => log.throwable("Database error", t)
    case Err.InternalError(t)        => log.throwable("Internal error", t)
  }

}
