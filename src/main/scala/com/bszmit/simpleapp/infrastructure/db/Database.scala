package com.bszmit.simpleapp.infrastructure.db

import cats.effect.Blocker
import com.bszmit.simpleapp.application.DBConfig
import doobie.hikari.HikariTransactor
import doobie.util.transactor.Transactor
import org.flywaydb.core.Flyway
import zio.blocking.Blocking
import zio.interop.catz.*
import zio.logging.*
import zio.{Has, Managed, RIO, RLayer, RManaged, Task, ZIO}

object Database {

  private def migrate(dbConfig: DBConfig): RIO[Logging, Unit] = {
    val migrateEffect = ZIO.effect {
      val flyway = Flyway.configure().dataSource(dbConfig.url, dbConfig.username, dbConfig.password).load()
      flyway.migrate()
    }

    for {
      _ <- log.info(s"Database ${dbConfig.url} migration is starting...")
      _ <- migrateEffect.tapError(log.throwable("Database migration failed", _))
      _ <- log.info(s"Database migration succeeded")
    } yield ()
  }

  private def makeTransactor(dbConfig: DBConfig): RManaged[Logging & Blocking, HikariTransactor[Task]] =
    ZIO.runtime[Blocking].toManaged_.flatMap { implicit rt =>
      for {
        _          <- log.info(s"Starting database with config: $dbConfig").toManaged_
        transactEC <- Managed.succeed(rt.environment.get[Blocking.Service].blockingExecutor.asEC)
        transactor <- HikariTransactor
          .newHikariTransactor[Task](
            dbConfig.driver,
            dbConfig.url,
            dbConfig.username,
            dbConfig.password,
            transactEC,
            Blocker.liftExecutionContext(transactEC),
          )
          .toManaged
      } yield transactor
    }

  def layer(conf: DBConfig): RLayer[Logging & Blocking, Has[Transactor[Task]]] = {
    val transactors = for {
      _  <- migrate(conf).toManaged_
      tx <- makeTransactor(conf)
    } yield tx

    transactors.toLayer
  }

}
