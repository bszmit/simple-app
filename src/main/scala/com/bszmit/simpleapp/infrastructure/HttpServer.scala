package com.bszmit.simpleapp.infrastructure

import fs2.Stream.Compiler.*
import org.http4s.HttpApp
import org.http4s.blaze.server.BlazeServerBuilder
import Utils.CtxOpsRIO
import zio.interop.catz.*
import zio.{RIO, ZEnv, ZIO}

object HttpServer {

  def runHttpServer[R](port: Int)(routes: HttpApp[RIO[R, *]]): RIO[ZEnv & R, Unit] =
    ZIO.runtime[ZEnv & R].flatMap { implicit rts =>
      BlazeServerBuilder
        .apply[ZIO[ZEnv & R, Throwable, *]](rts.platform.executor.asEC)
        .bindHttp(port, "0.0.0.0")
        .withHttpApp(routes.widen)
        .serve
        .compile
        .drain
    }

}
