package com.bszmit.simpleapp.infrastructure

import cats.data.EitherT
import cats.implicits.*
import cats.{Applicative, Monad}
import zio.{RIO, UIO, ZIO}

object Utils {

  implicit class TapLeftEitherT[F[_], A, B](e: EitherT[F, A, B])(implicit M: Monad[F], A: Applicative[F]) {
    def tapLeft(f: A => F[_]): EitherT[F, A, B] = EitherT {
      e.value.flatMap {
        case Left(err)    => f(err).flatMap(_ => A.pure(Left(err)))
        case Right(value) => A.pure(Right(value))
      }
    }
  }

  /** Some Http4s classes are invariant with respect to environment `R` and error `E` in `ZIO[R, E, ?]`.
    *
    * Example of the problem: Lets have a function `def foo(x: HttpRoutes[RIO[Logging & Blocking, *]]) = ???` Lets have a value which
    * requires only `Logging` `val v: HttpRoutes[RIO[Logging, *]] = ???` It makes sense to apply function `foo` to value `v`, because
    * function `foo` will provide richer (`Logging` and `Blocking`) environment that value `v` needs (just `Logging`). Unfortunately,
    * `HttpsRoutes` does not allow it, because it is invariant with respect to effect `RIO` and consequently also with respect to
    * environment `R`.
    *
    * Solution: We can safely cast value `v` from type `HttpRoutes[RIO[Logging], *]]` to type `HttpRoutes[RIO[Logging & Blocking], *]]`, By
    * this we're saying that `v` requires now richer environment then it actually does.
    */
  private def widenCtx[Ctx[G[_]], R, E, R2 <: R, E2 >: E](v: Ctx[ZIO[R, E, *]]): Ctx[ZIO[R2, E2, *]] = v.asInstanceOf[Ctx[ZIO[R2, E2, *]]]

  implicit class CtxOpsUIO[Ctx[G[_]]](resp: Ctx[UIO]) {
    def widen[R2, E2]: Ctx[ZIO[R2, E2, *]] = widenCtx(resp)
  }
  implicit class CtxOpsRIO[R, Ctx[G[_]]](resp: Ctx[RIO[R, *]]) {
    def widen[R2 <: R]: Ctx[RIO[R2, *]] = widenCtx(resp)
  }

}
