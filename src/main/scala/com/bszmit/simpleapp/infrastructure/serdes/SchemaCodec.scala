package com.bszmit.simpleapp.infrastructure.serdes

import com.bszmit.simpleapp.domain.Schema
import io.circe.Encoder

object SchemaCodec {
  val enc: Encoder[Schema] = _.v
}
