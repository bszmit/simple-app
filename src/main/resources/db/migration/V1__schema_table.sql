create table schemas
(
    id     varchar,
    schema jsonb,

    constraint schema_pkey primary key (id)
);
