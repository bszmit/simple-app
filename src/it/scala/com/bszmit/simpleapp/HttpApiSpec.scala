package com.bszmit.simpleapp

import com.bszmit.simpleapp.application.Layers
import com.bszmit.simpleapp.application.http.HttpRouter
import com.bszmit.simpleapp.domain.{Err, Schema, SchemaId, SchemaRepository}
import io.circe.Json
import io.circe.literal.JsonStringContext
import org.http4s.*
import org.http4s.circe.{jsonDecoder, jsonEncoder}
import zio.interop.catz.*
import zio.logging.Logging
import zio.magic.*
import zio.test.*
import zio.{Has, RIO, URIO, ZEnv, ZIO, ZLayer}

import java.util.UUID

// I'm using plain json to assert that API contract is not broken by accident in the future
object HttpApiSpec extends DefaultRunnableSpec with DockerContainers {

  type Eff[A] = RIO[Logging, A]

  val httpApp: URIO[Has[HttpRouter], HttpApp[Eff]]              = ZIO.service[HttpRouter].flatMap(r => ZIO.succeed(r.router))
  val schemaRepo: URIO[Has[SchemaRepository], SchemaRepository] = ZIO.service[SchemaRepository]

  val layer = ZLayer.fromSomeMagic[ZEnv, Logging & Has[SchemaRepository] & Has[HttpRouter]](
    Layers.loggerLayer,
    Layers.appConfig,
    Layers.repositoryLayer,
    Layers.serviceLayer,
    Layers.httpRoutesLayer,
  )

  val exampleSchema = Schema(json"""
                                    {
                                      "$$schema": "http://json-schema.org/draft-04/schema#",
                                      "type": "object",
                                      "properties": {
                                        "source": {
                                          "type": "string"
                                        },
                                        "destination": {
                                          "type": "string"
                                        },
                                        "timeout": {
                                          "type": "integer",
                                          "minimum": 0,
                                          "maximum": 32767
                                        },
                                        "chunks": {
                                          "type": "object",
                                          "properties": {
                                            "size": {
                                              "type": "integer"
                                            },
                                            "number": {
                                              "type": "integer"
                                            }
                                          },
                                          "required": ["size"]
                                        }
                                      },
                                      "required": ["source", "destination"]
                                    }
                                    """)

  def spec: Spec[zio.ZEnv, TestFailure[Throwable], TestSuccess] =
    suite("HttpApi")(
      testM("POST /schema/{schemaId} should save schema") {
        val id = SchemaId(UUID.randomUUID().toString)

        val saveSchemaReq = Request[Eff](method = Method.POST, uri = Uri.unsafeFromString(s"/schema/${id.v}")).withEntity(exampleSchema.v)

        for {
          response        <- httpApp >>= (_.run(saveSchemaReq))
          responseBody    <- response.as[Json]
          maybeReadSchema <- schemaRepo >>= (_.getById(id).orDieIgnore)
        } yield assertTrue(response.status == Status.Created) &&
          assertTrue {
            responseBody == json"""
                    {
                      "action": "uploadSchema",
                      "id": ${id.v},
                      "status": "success"
                    }
                """
          } &&
          assertTrue {
            maybeReadSchema match {
              case Some(readSchema) => readSchema == exampleSchema
              case None             => false
            }
          }
      },
      testM("POST /schema/{schemaId} should return error on invalid schema input") {
        val id = SchemaId(UUID.randomUUID().toString)

        val saveSchemaReq = Request[Eff](method = Method.POST, uri = Uri.unsafeFromString(s"/schema/${id.v}")).withEntity("{invalidJson...")

        for {
          response     <- httpApp >>= (_.run(saveSchemaReq))
          responseBody <- response.as[Json]
        } yield assertTrue(response.status == Status.BadRequest) &&
          assertTrue {
            responseBody == json"""{
              "action": "uploadSchema",
              "id": ${id.v},
              "status": "error",
              "message": "Invalid JSON"
            }"""
          }
      },
      testM("POST /validate/{schemaId} should validate document and drop null fields") {
        val id = SchemaId(UUID.randomUUID().toString)

        val document = json""" {
                  "source": "/home/alice/image.iso",
                  "destination": "/mnt/storage",
                  "timeout": null,
                  "chunks": {
                    "size": 1024,
                    "number": null
                  }
                }
              """

        val saveSchemaReq = Request[Eff](method = Method.POST, uri = Uri.unsafeFromString(s"/validate/${id.v}")).withEntity(document)

        for {
          _            <- schemaRepo >>= (_.register(id, exampleSchema).orDieIgnore)
          response     <- httpApp >>= (_.run(saveSchemaReq))
          responseBody <- response.as[Json]
        } yield assertTrue(response.status == Status.Created) &&
          assertTrue {
            responseBody == json"""
                {
                    "action": "validateDocument",
                    "id": ${id.v},
                    "status": "success",
                    "data":
                    {
                        "source": "/home/alice/image.iso",
                        "destination": "/mnt/storage",
                        "chunks": {
                            "size": 1024
                        }
                    }
                }
                """
          }
      },
      testM("POST /validate/{schemaId} should return detailed error on failed validation") {
        val id = SchemaId(UUID.randomUUID().toString)

        val document = json""" {
                  "source": "/home/alice/image.iso",
                  "timeout": null,
                  "chunks": {
                    "size": 1024,
                    "number": null
                  }
                }
              """

        val saveSchemaReq = Request[Eff](method = Method.POST, uri = Uri.unsafeFromString(s"/validate/${id.v}")).withEntity(document)

        for {
          _            <- schemaRepo >>= (_.register(id, exampleSchema).orDieIgnore)
          response     <- httpApp >>= (_.run(saveSchemaReq))
          responseBody <- response.as[Json]
        } yield assertTrue(response.status == Status.BadRequest) &&
          assertTrue {
            val c = responseBody.hcursor
            val eff = for {
              action  <- c.get[String]("action")
              _id     <- c.get[String]("id")
              status  <- c.get[String]("status")
              message <- c.get[List[Json]]("message")
            } yield action == "validateDocument" && _id == id.v && status == "error" && message.nonEmpty
            eff.getOrElse(false)
          }
      },
      testM("GET /schema/{schemaId} should fetch existing schema") {
        val id = SchemaId(UUID.randomUUID().toString)

        val getSchemaReq = Request[Eff](method = Method.GET, uri = Uri.unsafeFromString(s"/schema/${id.v}"))

        for {
          _            <- schemaRepo >>= (_.register(id, exampleSchema).orDieIgnore)
          response     <- httpApp >>= (_.run(getSchemaReq))
          responseBody <- response.as[Json]
        } yield assertTrue(response.status == Status.Ok) && assertTrue {
          responseBody == json"""
                    {
                      "action": "downloadSchema",
                      "id": ${id.v},
                      "status": "success",
                      "data": ${exampleSchema.v}
                    }
                """
        }
      },
      testM("GET /schema/{schemaId} should return 404 on non existing schema") {
        val id = SchemaId(UUID.randomUUID().toString)

        val getSchemaReq = Request[Eff](method = Method.GET, uri = Uri.unsafeFromString(s"/schema/${id.v}"))

        for {
          response     <- httpApp >>= (_.run(getSchemaReq))
          responseBody <- response.as[Json]
        } yield assertTrue(response.status == Status.NotFound) && assertTrue {
          responseBody == json"""
                    {
                      "action": "downloadSchema",
                      "id": ${id.v},
                      "status": "error",
                      "message": "Schema does not exist"
                    }
                """
        }
      },
    )
      .provideSomeLayerShared[ZEnv](layer.mapError(t => TestFailure.die(t)))

  implicit class dieOps[R, V](z: ZIO[R, Err, V]) {
    val orDieIgnore = z.orDieWith(_ => new Exception("unexpected error"))
  }
}
