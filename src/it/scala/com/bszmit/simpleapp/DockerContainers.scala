package com.bszmit.simpleapp

import com.dimafeng.testcontainers.DockerComposeContainer.*
import com.dimafeng.testcontainers.{DockerComposeContainer, ExposedService}
import org.testcontainers.containers.wait.strategy.Wait

import java.io.File

trait DockerContainers {
  locally {
    DockerContainers.start
  }
}

object DockerContainers {
  locally {
    val container = DockerComposeContainer(
      new File("src/it/resources/docker-compose.yml"),
      exposedServices = Seq(
        ExposedService(
          "postgres",
          5432,
          Wait.forLogMessage(".*database system is ready to accept connections.*\\s", 2),
        ),
      ),
    )
    container.start()
  }
  val start = "" // dummy variable, just to initialize object
}
