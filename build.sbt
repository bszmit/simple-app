ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.8"

lazy val root = (project in file("."))
  .configs(IntegrationTest)
  .settings(
    name := "simple-app",
    Defaults.itSettings,
  )

IntegrationTest / testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework")

libraryDependencies += "com.dimafeng"              %% "testcontainers-scala"  % "0.40.7" % "it"
libraryDependencies += "com.github.java-json-tools" % "json-schema-validator" % "2.2.14"
libraryDependencies += "com.typesafe"               % "config"                % "1.4.2"
libraryDependencies += "dev.zio"                   %% "zio"                   % "1.0.14"
libraryDependencies += "dev.zio"                   %% "zio-interop-cats"      % "2.5.1.0"
libraryDependencies += "dev.zio"                   %% "zio-logging"           % "0.5.14"
libraryDependencies += "dev.zio"                   %% "zio-logging-slf4j"     % "0.5.14"
libraryDependencies += "dev.zio"                   %% "zio-test"              % "1.0.14" % "it"
libraryDependencies += "dev.zio"                   %% "zio-test-sbt"          % "1.0.14" % "it"
libraryDependencies += "io.circe"                  %% "circe-core"            % "0.14.1"
libraryDependencies += "io.circe"                  %% "circe-literal"         % "0.14.1"
libraryDependencies += "io.circe"                  %% "circe-parser"          % "0.14.1"
libraryDependencies += "io.github.kitlangton"      %% "zio-magic"             % "0.3.12"
libraryDependencies += "org.apache.logging.log4j"   % "log4j-api"             % "2.17.2"
libraryDependencies += "org.apache.logging.log4j"   % "log4j-core"            % "2.17.2"
libraryDependencies += "org.apache.logging.log4j"   % "log4j-slf4j-impl"      % "2.17.2"
libraryDependencies += "org.flywaydb"               % "flyway-core"           % "8.4.2"
libraryDependencies += "org.http4s"                %% "http4s-blaze-server"   % "0.22.0"
libraryDependencies += "org.http4s"                %% "http4s-circe"          % "0.22.0"
libraryDependencies += "org.http4s"                %% "http4s-dsl"            % "0.22.0"
libraryDependencies += "org.tpolecat"              %% "doobie-core"           % "0.13.4"
libraryDependencies += "org.tpolecat"              %% "doobie-postgres"       % "0.13.4"
libraryDependencies += "org.tpolecat"              %% "doobie-postgres-circe" % "0.13.4"
libraryDependencies += "org.tpolecat"              %% "doobie-hikari"         % "0.13.4"
libraryDependencies += "org.typelevel"             %% "cats-core"             % "2.7.0"

addCompilerPlugin("org.typelevel" % "kind-projector" % "0.13.2" cross CrossVersion.full)

scalacOptions ++= Seq(
  "-deprecation",
  "-feature",
  "-unchecked",
  "-Xfatal-warnings",
  "-Ywarn-dead-code",
  "-Ywarn-value-discard",
  "-Ywarn-unused",
  "-language:higherKinds",
  "-Xsource:3",
)
